# BI-ZUM-PathFinding

This project was created as a homework for BI-ZUM. It uses Angular, TypeScript and Bootstrap.

## How to run

- Install node.js, angular cli
- From project root folder run command `ng serve`
