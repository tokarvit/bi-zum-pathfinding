import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ScrollingModule } from '@angular/cdk/scrolling';

import { AppComponent } from './app.component';
import { ControllingComponent } from './controlling/controlling.component';
import { LabyrinthComponent } from './labyrinth/labyrinth.component';
import { LabyrinthService } from 'src/services/labyrinth.service';

@NgModule({
  declarations: [AppComponent, ControllingComponent, LabyrinthComponent],
  imports: [BrowserModule, FormsModule, ScrollingModule],
  providers: [LabyrinthService],
  bootstrap: [AppComponent]
})
export class AppModule {}
