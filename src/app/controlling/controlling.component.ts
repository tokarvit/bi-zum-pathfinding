import { Component, OnInit } from '@angular/core';
import {
  LabyrinthService,
  LabyrinthServiceStatus
} from 'src/services/labyrinth.service';

@Component({
  selector: 'app-controlling',
  templateUrl: './controlling.component.html',
  styleUrls: ['./controlling.component.css']
})
export class ControllingComponent implements OnInit {
  searchAlgorythms: string[] = [
    'bfs',
    'random',
    'dfs',
    'greedy',
    'dijkstra',
    'a_star'
  ];
  currentAlgorythm: string = 'bfs';
  status: LabyrinthServiceStatus = LabyrinthServiceStatus.Empty;
  startButtonLabel: string = 'Start';

  constructor(private labyrinthService: LabyrinthService) {
    this.labyrinthService.statusChanged.subscribe(s => {
      this.status = s;
      switch (s) {
        case LabyrinthServiceStatus.Processing:
          this.startButtonLabel = 'Pause';
          break;
        case LabyrinthServiceStatus.Paused:
          this.startButtonLabel = 'Resume';
          break;
        default:
          this.startButtonLabel = 'Start';
      }
    });
  }

  ngOnInit() {}

  onStartBtn() {
    if (this.status == LabyrinthServiceStatus.Paused) {
      console.log('Resume');
      this.labyrinthService.resume();
    } else if (this.status == LabyrinthServiceStatus.Processing) {
      console.log('Pause');
      this.labyrinthService.pause();
    } else {
      console.log("Start using '" + this.currentAlgorythm + "'");
      this.labyrinthService.start(this.currentAlgorythm);
    }
  }

  onResetBtn() {
    console.log('Reset');
    this.labyrinthService.reset();
  }

  handleFileInput(files: FileList) {
    if (files.length == 0) return;

    this.labyrinthService.loadLabyrinth(files.item(0));
  }

  speedChange(speed: number) {
    this.labyrinthService.setSpeed(5 - speed);
  }
}
