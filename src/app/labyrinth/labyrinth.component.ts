import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input
} from '@angular/core';
import { LabyrinthService } from 'src/services/labyrinth.service';
import { Cell, CellStatus } from 'src/models/cell.model';

@Component({
  selector: 'app-labyrinth',
  templateUrl: './labyrinth.component.html',
  styleUrls: ['./labyrinth.component.css']
})
export class LabyrinthComponent implements OnInit {
  labyrinth: Cell[][];

  constructor(private labyrinthService: LabyrinthService) {
    this.labyrinthService.labyrinthChanged.subscribe(
      newLab => (this.labyrinth = newLab.labyrinth)
    );
  }

  ngOnInit() {
    this.labyrinth = this.labyrinthService.labyrinthInProcess.labyrinth;
  }

  getStatus(cell: Cell): string {
    return CellStatus[cell.state];
  }
}
