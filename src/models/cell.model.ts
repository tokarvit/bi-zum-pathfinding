import { Position } from './position.model';

export enum CellStatus {
  Obstacle,
  Opened,
  Closed,
  Path,
  End,
  Start,
  Empty
}

export class Cell {
  prev: Cell = null;
  value: number = Number.MAX_VALUE;

  constructor(public pos: Position, public state: CellStatus) {}

  copy(): Cell {
    var copy = new Cell(this.pos.copy(), this.state);
    copy.prev = this.prev;
    copy.value = this.value;

    return copy;
  }
}
