export class Position {
  constructor(public x: number, public y: number) {}

  copy(): Position {
    return new Position(this.x, this.y);
  }

  // getAround(maxPos: Position): Position[] {
  //   var arr: Position[] = [];

  //   if (this.x > 0) {
  //     if (this.y > 0) {
  //       arr.push(new Position(this.x - 1, this.y - 1));
  //     }
  //     arr.push(new Position(this.x - 1, this.y));
  //     if (maxPos.y > this.y) {
  //       arr.push(new Position(this.x - 1, this.y + 1));
  //     }
  //   }

  //   if (this.y > 0) {
  //     arr.push(new Position(this.x, this.y - 1));
  //   }
  //   if (maxPos.y > this.y) {
  //     arr.push(new Position(this.x, this.y + 1));
  //   }

  //   if (maxPos.x > this.x) {
  //     if (this.y > 0) {
  //       arr.push(new Position(this.x + 1, this.y - 1));
  //     }
  //     arr.push(new Position(this.x + 1, this.y));
  //     if (maxPos.y > this.y) {
  //       arr.push(new Position(this.x + 1, this.y + 1));
  //     }
  //   }

  //   return arr;
  // }

  getAround(maxPos: Position): Position[] {
    var arr: Position[] = [];

    if (this.x > 0) {
      arr.push(new Position(this.x - 1, this.y));
    }

    if (this.y > 0) {
      arr.push(new Position(this.x, this.y - 1));
    }
    if (maxPos.y > this.y) {
      arr.push(new Position(this.x, this.y + 1));
    }

    if (maxPos.x > this.x) {
      arr.push(new Position(this.x + 1, this.y));
    }

    return arr;
  }
}
