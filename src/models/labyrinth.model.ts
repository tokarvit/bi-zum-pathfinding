import { Cell, CellStatus } from './cell.model';
import { Position } from './position.model';

export class Labyrinth {
  labyrinth: Cell[][] = [];

  private startPosition: Position;
  private endPosition: Position;
  private maxPos: Position;

  constructor() {}

  getCopy() {
    console.log('Creating copy of labyrinth...');
    var newLab = new Labyrinth();

    newLab.labyrinth = new Array<Cell[]>(this.labyrinth.length);
    this.labyrinth.forEach((row, i) => {
      newLab.labyrinth[i] = new Array<Cell>(row.length);
      row.forEach((cell, j) => {
        newLab.labyrinth[i][j] = cell.copy();
      });
    });

    newLab.startPosition = this.startPosition.copy();
    newLab.endPosition = this.endPosition.copy();
    newLab.maxPos = this.maxPos.copy();

    console.log('Finish creating copy of labyrinth');
    return newLab;
  }

  loadDummy() {
    var dummyLab = [
      'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
      'X         X   X                 X',
      'X XXX X   X X   XXXXX X   XXX X X',
      'X     X X   X X   X   X X   X   X',
      'X  XXXX   XXX XXX X  XXX X  X   X',
      'X     X         X X         X   X',
      'X  XX  XX XX XX X X X XXX XXXX  X',
      'X   X     X       X             X',
      'X X  XX     X XXX XXXXX X X X X X',
      'X   X     X             X   X   X',
      'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
      'start 3, 1',
      'end 17, 7'
    ];

    this.parseLines(dummyLab);
  }

  getNeighbors(cell: Cell): Cell[] {
    var neighbors: Cell[] = [];
    cell.pos
      .getAround(this.maxPos)
      .forEach(p => neighbors.push(this.getCellByPos(p)));

    return neighbors;
  }

  getCell(x: number, y: number): Cell {
    return this.labyrinth[y][x];
  }

  getCellByPos(pos: Position): Cell {
    return this.getCell(pos.x, pos.y);
  }

  getTargetCell(): Cell {
    return this.getCellByPos(this.endPosition);
  }

  getStartCell(): Cell {
    return this.getCellByPos(this.startPosition);
  }

  getDistanceFromTarget(a: Cell): number {
    return this.getDistance(a, this.getTargetCell());
  }

  getDistance(a: Cell, b: Cell): number {
    return Math.sqrt(
      Math.pow(a.pos.x - b.pos.x, 2) + Math.pow(a.pos.y - b.pos.y, 2)
    );
  }

  async parseFile(file: File) {
    var reader = new FileReader();

    var promise = new Promise<string[]>((resolve, reject) => {
      reader.onloadend = e => {
        let file = reader.result as string;
        resolve(file.split(/\r\n|\n/));
      };
    });

    reader.readAsText(file);

    var allLines = await promise;
    if (allLines[allLines.length - 1].length == 0) {
      allLines = allLines.slice(0, -1);
    }
    this.parseLines(allLines);
  }

  private parseLines(lines: string[]) {
    this.labyrinth = new Array<Cell[]>(lines.length - 2);
    // Reading line by line
    lines.forEach((line, i) => {
      if (line.length != 0) {
        // console.log("Parsing line '" + line + "'");
        this.parseLine(line, i);
      }
    });

    this.maxPos = new Position(
      this.labyrinth[0].length - 1,
      this.labyrinth.length - 1
    );

    console.log('Parsing ended');
  }

  private parseLine(line: string, y: number) {
    // console.log("Line to parse: '" + line + "'");

    if (line.charAt(0) == 'X') {
      this.parseHorizontal(line, y);
    } else if (line.charAt(0) == 's') {
      this.startPosition = this.getPositionFromLine(line);
      this.getStartCell().state = CellStatus.Start;
    } else if (line.charAt(0) == 'e') {
      this.endPosition = this.getPositionFromLine(line);
      this.getTargetCell().state = CellStatus.End;
    } else {
      alert("Unknown start of line '" + line.charAt(1) + "'");
    }
  }

  private parseHorizontal(line: string, y: number) {
    var newHorizontal: Cell[] = new Array<Cell>(line.length);

    for (var i = 0; i < line.length; i++) {
      var ch = line.charAt(i);
      switch (ch) {
        case 'X':
          newHorizontal[i] = new Cell(new Position(i, y), CellStatus.Obstacle);
          break;
        case ' ':
          newHorizontal[i] = new Cell(new Position(i, y), CellStatus.Empty);
          break;
        default:
          alert("Unknown char '" + ch + "'");
      }
    }

    this.labyrinth[y] = newHorizontal;
  }

  private getPositionFromLine(line: string): Position {
    var splitted = line.split(' ');
    var x = splitted[1].slice(0, -1);
    var y = splitted[2];

    return new Position(Number(x), Number(y));
  }

  private memorySizeOf(obj) {
    var bytes = 0;

    function sizeOf(obj) {
      if (obj !== null && obj !== undefined) {
        switch (typeof obj) {
          case 'number':
            bytes += 8;
            break;
          case 'string':
            bytes += obj.length * 2;
            break;
          case 'boolean':
            bytes += 4;
            break;
          case 'object':
            var objClass = Object.prototype.toString.call(obj).slice(8, -1);
            if (objClass === 'Object' || objClass === 'Array') {
              for (var key in obj) {
                if (!obj.hasOwnProperty(key)) continue;
                sizeOf(obj[key]);
              }
            } else bytes += obj.toString().length * 2;
            break;
        }
      }
      return bytes;
    }

    function formatByteSize(bytes) {
      if (bytes < 1024) return bytes + ' bytes';
      else if (bytes < 1048576) return (bytes / 1024).toFixed(3) + ' KiB';
      else if (bytes < 1073741824) return (bytes / 1048576).toFixed(3) + ' MiB';
      else return (bytes / 1073741824).toFixed(3) + ' GiB';
    }

    return formatByteSize(sizeOf(obj));
  }
}
