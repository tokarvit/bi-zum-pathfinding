import { Cell, CellStatus } from 'src/models/cell.model';
import { Injectable } from '@angular/core';
import { Position } from 'src/models/position.model';
import { Subject, of, interval } from 'rxjs';
import { delayWhen, delay } from 'rxjs/operators';
import { Labyrinth } from 'src/models/labyrinth.model';
import { setRootDomAdapter } from '@angular/platform-browser/src/dom/dom_adapter';

export enum LabyrinthServiceStatus {
  Empty,
  Init,
  Processing,
  Paused,
  Finished
}

[Injectable()];
export class LabyrinthService {
  private labyrinth: Labyrinth;

  labyrinthInProcess: Labyrinth;
  labyrinthChanged = new Subject<Labyrinth>();
  statusChanged = new Subject<LabyrinthServiceStatus>();
  status: LabyrinthServiceStatus = LabyrinthServiceStatus.Empty;
  currentAlgorythm: string = 'bfs';
  speed: number = 1;
  nodeExpanded: number = 0;
  pathLength: number = 0;

  constructor() {
    this.labyrinth = new Labyrinth();
    this.labyrinth.loadDummy();
    this.reset();
  }

  async loadLabyrinth(file: File) {
    var newLab = new Labyrinth();
    await newLab.parseFile(file);
    this.labyrinth = newLab;
    this.reset();
  }

  setStatus(status: LabyrinthServiceStatus) {
    this.status = status;
    this.statusChanged.next(status);
  }

  setSpeed(speed: number) {
    this.speed = speed;
    console.log('Speed has been setted to ' + speed);
  }

  reset() {
    this.labyrinthInProcess = this.labyrinth.getCopy();
    this.setStatus(LabyrinthServiceStatus.Init);
    this.labyrinthChanged.next(this.labyrinthInProcess);
  }

  pause() {
    this.setStatus(LabyrinthServiceStatus.Paused);
  }

  resume() {
    this.setStatus(LabyrinthServiceStatus.Processing);
  }

  async start(algorythm: string) {
    if (this.status != LabyrinthServiceStatus.Init) this.reset();
    this.currentAlgorythm = algorythm;
    this.nodeExpanded = 0;
    this.pathLength = 0;

    this.setStatus(LabyrinthServiceStatus.Processing);
    if (await this.simplePathFinding()) {
      console.log('Target cell has been found');

      var curCell: Cell = this.labyrinthInProcess.getTargetCell();
      while (true) {
        curCell = curCell.prev;
        if (curCell == null || curCell.state == CellStatus.Start) break;
        curCell.state = CellStatus.Path;
        this.pathLength++;
      }
    } else {
    }
    this.setStatus(LabyrinthServiceStatus.Finished);

    this.finishAlert();
  }

  private async simplePathFinding(): Promise<boolean> {
    var startCell = this.labyrinthInProcess.getStartCell();
    startCell.value = 0;
    var queue = [startCell];
    var breakWithDelay = false;

    while (queue.length > 0) {
      let currCell = this.getNextCell(queue);
      let neighbors: Cell[] = this.labyrinthInProcess.getNeighbors(currCell);

      for (var i = 0; i < neighbors.length; i++) {
        let childCell = neighbors[i];

        if (childCell.state == CellStatus.End) {
          childCell.prev = currCell;
          breakWithDelay = this.delayBreakIfFindTarget();
          if (!breakWithDelay) {
            return true;
          }
        }

        this.operateNeighbor(currCell, childCell);

        if (childCell.state == CellStatus.Empty) {
          childCell.state = CellStatus.Opened;
          queue.push(childCell);
          this.nodeExpanded++;
        }
      }

      if (currCell.state != CellStatus.Start) {
        currCell.state = CellStatus.Closed;
      }

      if (breakWithDelay) {
        return true;
      }

      await this.waitUntilNextStep();
      if (this.status != LabyrinthServiceStatus.Processing) {
        return false;
      }
    }

    return false;
  }

  private async waitUntilNextStep() {
    if (this.speed > 0.25) {
      await this.sleep(this.speed * 1000);
    } else if (this.speed > 0.00001) {
      await this.sleep(50);
    }
    await this.waitUntil(() => this.status != LabyrinthServiceStatus.Paused);
  }

  private getNextCell(queue: Cell[]): Cell {
    switch (this.currentAlgorythm) {
      case 'bfs':
        return queue.shift();
      case 'random':
        return queue.splice(this.getRandomInt(queue.length), 1)[0];
      case 'dfs':
        return queue.pop();

      case 'greedy':
        return this.getNextGreedy(queue);
      case 'dijkstra':
        return this.getNextDejkstra(queue);
      case 'a_star':
        return this.getNextAStar(queue);

      default:
        alert("Unknown algorythm '" + this.currentAlgorythm + "'");
    }
  }

  private operateNeighbor(parent: Cell, child: Cell) {
    if (
      this.currentAlgorythm == 'dijkstra' ||
      this.currentAlgorythm == 'a_star'
    ) {
      var newValue = parent.value + 1;
      if (newValue < child.value) {
        child.value = newValue;
        child.prev = parent;
      }
    } else {
      if (child.state == CellStatus.Empty) {
        child.prev = parent;
      }
    }
  }

  private delayBreakIfFindTarget(): boolean {
    switch (this.currentAlgorythm) {
      case 'dijkstra':
      case 'a_star':
        return false;

      default:
        return true;
    }
  }

  private getNextDejkstra(queue: Cell[]): Cell {
    queue.sort((a, b) => a.value - b.value);
    return queue.shift();
  }

  private getNextGreedy(queue: Cell[]): Cell {
    queue.sort(
      (a, b) =>
        this.labyrinthInProcess.getDistanceFromTarget(a) -
        this.labyrinthInProcess.getDistanceFromTarget(b)
    );
    return queue.shift();
  }

  private getNextAStar(queue: Cell[]): Cell {
    queue.sort(
      (a, b) =>
        a.value +
        this.labyrinthInProcess.getDistanceFromTarget(a) -
        (b.value + this.labyrinthInProcess.getDistanceFromTarget(b))
    );
    return queue.shift();
  }

  private sleep(ms: number): Promise<any> {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  private async waitUntil(condition: () => boolean): Promise<any> {
    while (!condition()) {
      await this.sleep(50);
    }
  }

  private finishAlert() {
    alert(
      'Nodes expaned: ' +
        this.nodeExpanded +
        '\n' +
        'Path length: ' +
        this.pathLength
    );
  }

  private getRandomInt(max: number) {
    return Math.floor(Math.random() * max);
  }
}
